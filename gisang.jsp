<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ page import= "org.apache.http.HttpEntity" %>
<%@ page import= "org.apache.http.HttpResponse" %>
<%@ page import= "org.apache.http.NameValuePair" %>
<%@ page import= "org.apache.http.ParseException" %>
<%@ page import= "org.apache.http.client.HttpClient" %>
<%@ page import= "org.apache.http.client.entity.UrlEncodedFormEntity" %>
<%@ page import= "org.apache.http.client.methods.HttpGet" %>
<%@ page import= "org.apache.http.client.methods.HttpPost" %>
<%@ page import= "org.apache.http.impl.client.DefaultHttpClient" %>
<%@ page import= "org.apache.http.message.BasicNameValuePair" %>
<%@ page import= "org.apache.http.params.HttpConnectionParams" %>
<%@ page import= "org.apache.http.util.EntityUtils" %>
<%@ page import= "org.apache.http.conn.ClientConnectionManager" %>
<%@ page import= "org.apache.http.params.HttpParams" %>
<%@ page import= "org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager" %>
<%@ page import= "java.io.*,java.text.*,java.net.*,java.util.*,java.sql.*,javax.servlet.*,javax.sql.*,javax.naming.*" %>
<%@ page import= "javax.xml.parsers.*,org.w3c.dom.*,org.xml.sax.*" %>

<html>
<head>
<link rel="stylesheet" href="weather_icons_master/css/weather-icons.css">
<link rel="stylesheet" href="weather_icons_master/css/weather-icons-wind.css">

<style>
.theme {color:#ffffff; background-color:#000000}
.th {text-align: left; padding-left: 8px;}
tr{border-bottom: solid 1px #000000;}
td{width: 150px; height: 40px;}
i{font-size: 1.5em;}

</style>
<title>기상예보</title>
</head>
<body>
<h1>기상 예보 / Weather Forcasting</h1>
<%
	
	String[] seq = new String[18];		// 48시간 중 몇번째 인지 가르킴
	String[] hour = new String[18];		// 동테예보 3시간 단위
	String[] day = new String[18];		// 1번째날 (0: 오늘 , 1: 내일 , 2: 모레)
	String[] temp = new String[18];		// 현재 시간 온도
	String[] tmx = new String[18];		// 최고 온도
	String[] tmn = new String[18];		// 최저 온도
	String[] sky = new String[18];		// 하늘 상태 코드 (1: 맑음, 2: 구름조금, 3: 구름많음, 4: 흐림)
	String[] pty = new String[18];		// 강수 상태코드 (0: 없음, 1: 비, 2: 비/눈, 3: 눈/비, 4: 눈)
	String[] wfKor = new String[18];		// 날씨 한국어
	String[] wfEn = new String[18];		// 날씨 영어
	String[] pop = new String[18];		// 강수 확률
	String[] r12 = new String[18];		// 12시간 예상 강수량
	String[] s12 = new String[18];		// 12시간 예상 적설량
	String[] ws = new String[18];			// 풍속 (m/s)
	String[] wd = new String[18];			// 풍향 (0~7: 북, 북동, 동, 남동, 남, 남서, 서, 북서)
	String[] wdKor = new String[18];		// 풍향 한국어
	String[] wdEn = new String[18];		// 풍향 영어
	String[] reh = new String[18];		// 습도 (%)
	String[] r06 = new String[18];		// 6시간 예상 강수량
	String[] s06 = new String[18];		// 6시간 예상 적설량	
	
	
		// DocumentBuilderFactory 객체생성		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		// 문자열 선언 및 해당 URL 주소 저장
		String urlstr = "http://www.kma.go.kr/wid/queryDFS.jsp?gridx=61&gridy=123";
		
		// 해당 URL 주소를 가지는 URL 인스턴스 생성 후 URL 형식의 변수 url에 초기값으로 설정
		URL url = new URL(urlstr);
		HttpURLConnection urlconnection = (HttpURLConnection)url.openConnection();
		// HttpURLConnection 의 연결 설정
		
		// 버퍼읽기로 연결한 url을 읽는다
		BufferedReader br = new BufferedReader(new InputStreamReader(urlconnection.getInputStream(), "UTF-8"));
			
			// 변수 선언 및 초기값 설정
			String result = "";
			String line;
			
			// 읽는 결과값이 존재하는 경우 반복문 실행
			while((line = br.readLine()) != null){
				result = result + line.trim();	// result = URL로 XML을 읽은 값
			}
		
		// 결과값을 읽어 이를 InputSource 변수로 설정
		InputSource is = new InputSource(new StringReader(result));
		
		// 문서 생성
		Document doc = builder.parse(is);
		
		// 생성된 document에서 각 요소들을 접근하여 데이터를 저장함
		Element root = doc.getDocumentElement();	// root태그를 가져오기도 하지만 이 소스에서는 쓰이는 곳이 없다.
		NodeList tag_001 = doc.getElementsByTagName("data");	// xml의 루트를 기준으로 data 태그를 찾는다.
%>
<%		
		for(int i = 0; i < tag_001.getLength(); i++){
			
			// data 태그가 여러개 있는데 하나를 선택 item(i) 하여 attribute가 seq를 찾고 그것의 값을 찾는다.
			// data seq="0", data seq="1" 등을 보고 결국 0,1,2,3 값이 나온다.
			// seq=tag_001.item(i).getAttributes().getNamedItem("seq").getNodeValue();
			
			Element elmt = (Element)tag_001.item(i);
			
			seq[i]=tag_001.item(i).getAttributes().getNamedItem("seq").getNodeValue();
			
			hour[i] = elmt.getElementsByTagName("hour").item(0).getFirstChild().getNodeValue()+"";
			day[i] = elmt.getElementsByTagName("day").item(0).getFirstChild().getNodeValue()+"";
			temp[i] = elmt.getElementsByTagName("temp").item(0).getFirstChild().getNodeValue();
			tmx[i] = elmt.getElementsByTagName("tmx").item(0).getFirstChild().getNodeValue();
			tmn[i] = elmt.getElementsByTagName("tmn").item(0).getFirstChild().getNodeValue();
			sky[i] = elmt.getElementsByTagName("sky").item(0).getFirstChild().getNodeValue();
			pty[i] = elmt.getElementsByTagName("pty").item(0).getFirstChild().getNodeValue();
			wfKor[i] = elmt.getElementsByTagName("wfKor").item(0).getFirstChild().getNodeValue();
			wfEn[i] = elmt.getElementsByTagName("wfEn").item(0).getFirstChild().getNodeValue();
			pop[i] = elmt.getElementsByTagName("pop").item(0).getFirstChild().getNodeValue();
			r12[i] = elmt.getElementsByTagName("r12").item(0).getFirstChild().getNodeValue();
			s12[i] = elmt.getElementsByTagName("s12").item(0).getFirstChild().getNodeValue();
			ws[i] = elmt.getElementsByTagName("ws").item(0).getFirstChild().getNodeValue();
			wd[i] = elmt.getElementsByTagName("wd").item(0).getFirstChild().getNodeValue();
			wdKor[i] = elmt.getElementsByTagName("wdKor").item(0).getFirstChild().getNodeValue();
			wdEn[i] = elmt.getElementsByTagName("wdEn").item(0).getFirstChild().getNodeValue();
			reh[i] = elmt.getElementsByTagName("reh").item(0).getFirstChild().getNodeValue();
			r06[i] = elmt.getElementsByTagName("r06").item(0).getFirstChild().getNodeValue();
			s06[i] = elmt.getElementsByTagName("s06").item(0).getFirstChild().getNodeValue();
		}
		
	String bgcolor = "#ffffff";
%>
<table cellspacing=0 border=0 style="border-top: solid 1px #000000; border-bottom: solid 1px #000000; border-left: solid 1px #000000;text-align: center;">
	
	<tr style="border-bottom: solid 1px #000000;">
		<td>48시간 순서</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		out.println("<td style='background-color: "+bgcolor+";'>"+seq[i]+"</td>");			
	}
%>
	</tr>
	<tr><td class="theme th">하루</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){
			bgcolor="#ffffff";
			out.println("<td style='background-color: "+bgcolor+";'>오늘</td>");
		}
		else if ((day[i]+"").equals("1")){
			bgcolor="#e6e6e6"; 
			out.println("<td style='background-color: "+bgcolor+";'>내일</td>");
		}
		else if ((day[i]+"").equals("2")){
			bgcolor="#bdbdbd"; 
			out.println("<td style='background-color: "+bgcolor+";'>모레</td>");
		}
		
	}
%>
	</tr>
	<tr><td class="theme th">시간</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}	
		out.println("<td style='background-color: "+bgcolor+";'>"+hour[i]+":00</td>");					
	}
%>
	</tr>	
	<tr><td class="theme th">현재온도(℃)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}		
		out.println("<td style='background-color: "+bgcolor+";'>"+temp[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">최고온도(℃)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}	
		if((tmx[i]+"").equals("-999.0")){tmx[i]="-";}
		out.println("<td style='background-color: "+bgcolor+";'>"+tmx[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">최저온도(℃)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		if((tmn[i]+"").equals("-999.0")){tmn[i]="-";}
		out.println("<td style='background-color: "+bgcolor+";'>"+tmn[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">하늘상태</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}			
		out.println("<td style='background-color: "+bgcolor+";'>");
		
		String skyS = sky[i];
		if (skyS.equals("1")){
			out.println("<i class='wi wi-day-sunny'></i>");
//			out.println("<img src='pic/weather_icon-01.png'></img>");
		} else if (skyS.equals("2") || skyS.equals("3")){
			out.println("<i class='wi wi-cloud'></i>");
//			out.println("<img src='pic/weather_icon-71.png'></img>");
		} else if (skyS.equals("4")){
			out.println("<i class='wi wi-cloudy'></i>");
		}
		
		out.println("</td>");
//		out.println("<td style='background-color: "+bgcolor+";'>"+sky[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">강수상태</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
//		out.println("<td style='background-color: "+bgcolor+";'>"+pty[i]+"</td>");
	
		out.println("<td style='background-color: "+bgcolor+";'>");
		
		String pytS = pty[i]; 
		if (pytS.equals("0")){
			out.println("-");
		} else if (pytS.equals("1")){
			out.println("<i class='wi wi-rain'></i>");
		} else if (pytS.equals("2")){
			out.println("<i class='wi wi-rain-mix'></i>");
		} else if (pytS.equals("3")){
			out.println("<i class='wi wi-rain-mix'></i>");
		} else if (pytS.equals("4")){
			out.println("<i class='wi wi-snow'></i>");
		} else {
			out.println("-");
		}
		out.println("</td>");
	}	
%>
	</tr>
	<tr><td class="theme th">한국어</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+wfKor[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">Eng</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+wfEn[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">강수확률(%)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+pop[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">12시간 예상 강수량</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+r12[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">12시간 예상 적설량</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+s12[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">풍속(m/s)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		String wsS = ws[i];
		if (wsS.length() > 3){wsS = wsS.substring(0,3);}		
		out.println("<td style='background-color: "+bgcolor+";'>"+wsS+"</td>");
//		out.println("<td style='background-color: "+bgcolor+";'>"+ws[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">풍향</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		
		String wdS = wd[i];
		String windImage = "";
		if(wdS.equals("0")){windImage="wi-wind wi-from-n";}
		if(wdS.equals("1")){windImage="wi-wind wi-from-ne";}
		if(wdS.equals("2")){windImage="wi-wind wi-from-e";}
		if(wdS.equals("3")){windImage="wi-wind wi-from-se";}
		if(wdS.equals("4")){windImage="wi-wind wi-from-s";}
		if(wdS.equals("5")){windImage="wi-wind wi-from-sw";}
		if(wdS.equals("6")){windImage="wi-wind wi-from-w";}
		if(wdS.equals("7")){windImage="wi-wind wi-from-nw";}
		
		out.println("<td style='background-color: "+bgcolor+";'>");
		out.println("<i class='wi "+windImage+"'></i>");
//		out.println("<td style='background-color: "+bgcolor+";'>"+wd[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">풍향</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+wdKor[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">Wind Direction</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+wdEn[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">습도(%)</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+reh[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">6시간 예상 강수량</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+r06[i]+"</td>");					
	}
%>
	</tr>
	<tr><td class="theme th">6시간 예상 적설량</td>
<%
	for(int i = 0; i < tag_001.getLength(); i++){
		if ((day[i]+"").equals("0")){bgcolor="#ffffff";}
		else if ((day[i]+"").equals("1")){bgcolor="#e6e6e6";}
		else if ((day[i]+"").equals("2")){bgcolor="#bdbdbd";}
		out.println("<td style='background-color: "+bgcolor+";'>"+s06[i]+"</td>");					
	}
%>
	</tr>		
</table>

</body>
</html>